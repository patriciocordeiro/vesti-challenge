// This file can be replaced during build by using the `fileReplacements` array.
// `ng build --prod` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.

export const environment = {
  production: false,
  firebase: {
    apiKey: 'AIzaSyDxebAE0-GtWgEJJwv2m2ZB8tonIJLUmqw',
    authDomain: 'vesti-d2c4c.firebaseapp.com',
    projectId: 'vesti-d2c4c',
    storageBucket: 'vesti-d2c4c.appspot.com',
    messagingSenderId: '465225267673',
    appId: '1:465225267673:web:604e914239b80783b401c8',
    measurementId: 'G-4180HBMK1W'
  }
};

/*
 * For easier debugging in development mode, you can import the following file
 * to ignore zone related error stack frames such as `zone.run`, `zoneDelegate.invokeTask`.
 *
 * This import should be commented out in production mode because it will have a negative impact
 * on performance if an error is thrown.
 */
// import 'zone.js/dist/zone-error';  // Included with Angular CLI.
