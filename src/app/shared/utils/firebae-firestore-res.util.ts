import { Subscriber } from 'rxjs';

import { getErrorMessage } from './map-firebase-error-message.util';

export const dbResponseHandler = (res: any, observer: Subscriber<any>) => {
  if (res && res.error) {
    observer.error(getErrorMessage(res.error).generalError);
  } else {
    if (!Array.isArray(res)) {
      observer.next([res]);
    } else {
      observer.next(res);
    }
  }
  return observer;

};
