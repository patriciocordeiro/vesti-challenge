import firebase from 'firebase';
import { from, Observable } from 'rxjs';
import { map, take, toArray } from 'rxjs/operators';

export const mapItemsWithId = <T>(
  items: firebase.firestore.DocumentSnapshot[] | firebase.firestore.QueryDocumentSnapshot[]
): Observable<T[]> => from(items).pipe(
  map(item => {
    let myItem: any = {};
    myItem = item.data();
    myItem.id = item.id;
    return myItem;
  }),
  take(items.length),
  toArray()
);
