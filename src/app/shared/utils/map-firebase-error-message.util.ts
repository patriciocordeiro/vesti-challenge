import firebase from 'firebase';

import { GeneralError } from './../interfaces/general-error';

export const getErrorMessage = (
  error: firebase.FirebaseError
): { [key: string]: GeneralError} => ({
  createError: {
    code: '002',
    message: 'Erro ao adicionar os dados'
  },
  updateError: {
    code: '003',
    message: 'Erro ao atualizar os dados'
  },
  deleteError: {
    code: '004',
    message: 'Dados não encontrados'
  },

  generalError: {
    code: '005',
    message: `Ocorreu um erro desconhecido:' ' ${error && error.message}`
  },
  notFoundError: {
    code: '001',
    message: 'Dados não encontrados'
  }

});
