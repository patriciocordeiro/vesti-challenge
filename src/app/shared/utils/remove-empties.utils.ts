export const removerEmptyAndNull = (object: any) => {
  const res = {} as any;
  if (object) {
    for (const key in object) {
      if (Object.hasOwnProperty.call(object, key)) {
        if ((object[key] != null) || (object[key] !== undefined)) {
          res[key] = object[key];
        }
      }
    }

    return res;
  }
  return object;
};
