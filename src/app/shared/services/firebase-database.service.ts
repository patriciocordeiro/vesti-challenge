import { Inject } from '@angular/core';
import { AngularFirestore, CollectionReference, DocumentData, Query } from '@angular/fire/firestore';
import firebase from 'firebase/app';
import { from, Observable, of } from 'rxjs';
import { catchError, map, mergeMap, toArray } from 'rxjs/operators';

import { PageSort } from '../interfaces/page-sort';
import { dbResponseHandler } from '../utils/firebae-firestore-res.util';
import { getErrorMessage } from '../utils/map-firebase-error-message.util';
import { mapItemsWithId } from '../utils/map-item-with-id.util';
import { removerEmptyAndNull } from '../utils/remove-empties.utils';

export abstract class FirebaseDatabaseService {

  sortDefault: PageSort;
  collection: CollectionReference<DocumentData>;
  private databaseName: string;

  constructor(
    public db: AngularFirestore,
    @Inject(String) public dbName: string,
    @Inject('PageSort') public mSortDefault: PageSort,
  ) {
    this.databaseName = dbName;
    this.collection = this.db.collection<DocumentData>(this.databaseName).ref;
    this.sortDefault = mSortDefault;
  }


  create<T>(data: any): Observable<T> {
    data = {
      ...data,
      ...{
        createdAt: firebase.firestore.FieldValue.serverTimestamp(),
        updatedAt: firebase.firestore.FieldValue.serverTimestamp()
      }
    };

    return new Observable(observer => {
      from(this.collection.add(data))
        .pipe(
          mergeMap(item => from(item.get())),
          map(item => {
            const res: any = item.data();
            res.id = item.id;
            return res;
          }),
          catchError(error => of({ error }))
        )
        .subscribe((res: any) => dbResponseHandler(res, observer));
    });
  }

  createWithCustomId(data: any, customId: string) {
    data = {
      ...removerEmptyAndNull(data),
      ...{
        createdAt: firebase.firestore.FieldValue.serverTimestamp(),
        updatedAt: firebase.firestore.FieldValue.serverTimestamp()
      }
    };
    return new Observable(observer => {
      from(this.collection.doc(customId).set(data))
        .pipe(
          map(mdata => mdata),
          catchError(error => of({ error }))
        )
        .subscribe((res: any) => dbResponseHandler(res, observer));
    });
  }

  update(id: string, query: any): Observable<string | any> {

    query = {
      ...removerEmptyAndNull(query),
      ...{ updatedAt: firebase.firestore.FieldValue.serverTimestamp() }
    };

    return from(this.collection.doc(id).update(query)).pipe(
      map(() => id),
      catchError(error => of({ error }))
    );
  }

  delete(id: string): Observable<any> {
    return from(this.collection.doc(id).delete()).pipe(
      map(() => 'deleted'),
      catchError(error => of(getErrorMessage(error).generalError))
    );
  }


  getOne<T>(id: string): Observable<T[]> {
    return new Observable(observer => {
      from(this.collection.doc(id).get())
        .pipe(
          map(snapshot => {
            const res: any = snapshot.data();
            res.id = snapshot.id;
            return res;
          }),
          toArray(),
          catchError(error => of({ error })
          )
        )

        .subscribe((res: any) => {
          dbResponseHandler(res, observer);
        });
    });
  }



  watchAll<T>(): Observable<T[]> {
    return new Observable(observer =>
      this.collection.onSnapshot(value =>
        from(value.docs)
          .pipe(
            map(items => {
              const data: any = items.data();
              data.id = items.id;
              return data;
            }),
            map(items => items),
            catchError(error => of({ error })),
            toArray()
          )
          .subscribe((res: any[]) => {
            dbResponseHandler(res, observer);
          })
      )
    );
  }


  watchOneItem<T>(id: string): Observable<T> {
    return new Observable(observer =>
      this.collection.doc(id).onSnapshot(snapshot => {
        let data: any = snapshot.data();
        data = { ...data, ...{ id: snapshot.id } };
        observer.next(data as T);
      })
    );
  }


  getAll<T>(query: any): Observable<T[]> {
    return new Observable(observer =>
      from(this.collection.orderBy(query.sort.active, query.sort.direction).get())
        .pipe(
          map(items => items.docs),
          mergeMap(items => mapItemsWithId(items)),
          catchError(error => of(error))
        )
        .subscribe((res: any) => dbResponseHandler(res, observer))
    );
  }



  search<T>(text: string): Observable<T[]> {
    let query: Query = this.collection;


    query = query.where('tags', 'array-contains', text);


    return new Observable(observer => from(query.get())
      .pipe(
        map(val => val.docs),
        mergeMap(docs => mapItemsWithId<T>(docs)),
        catchError(error => of({ error }))
      )
      .subscribe((res: any) => {
        if (res?.error) {
          observer.error(res.error);

        } else {
          observer.next(res);
        }

      }));
  };











};
