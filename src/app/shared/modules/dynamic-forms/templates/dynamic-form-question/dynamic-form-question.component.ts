import { Component, Input, OnInit } from '@angular/core';
import { FormGroup } from '@angular/forms';

import { QuestionBaseComponent } from '../../factory/question-base';

@Component({
  selector: 'app-dynamic-form-question',
  templateUrl: './dynamic-form-question.component.html',
  styleUrls: ['./dynamic-form-question.component.scss']
})
export class DynamicFormQuestionComponent implements OnInit {


  @Input() question: QuestionBaseComponent;
  @Input() form: FormGroup;


  get isValid() {
    return this.form.controls[this.question.key].valid;
  }

  constructor() { }

  ngOnInit(): void {
  }
}
