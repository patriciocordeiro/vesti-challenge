import { Component, Inject } from '@angular/core';
import { Validators } from '@angular/forms';

import { QuestionBaseOption } from '../interfaces/question-base-option';

@Component({
  template: ''
})
export abstract class QuestionBaseComponent implements QuestionBaseOption {

  value: any;
  key: string;
  label: string;
  order: number;
  controlType: string;
  disabled: boolean;
  position: string;
  placeholder?: string;
  mask?: string;
  validator?: Validators;
  format?: string;

  constructor(
    @Inject({}) options: QuestionBaseOption
  ) {
    this.value = options.value;
    this.key = options.key || '';
    this.label = options.label || '';
    this.disabled = !!options.disabled;
    this.order = options.order === undefined ? 1 : options.order;
    this.controlType = options.controlType || '';
    this.position = options.position || 'stacked';
    this.placeholder = options.placeholder || '';
    this.mask = options.mask || '';
    this.validator = options.validator || null;
  }


}
