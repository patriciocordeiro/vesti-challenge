import { QuestionBaseOption } from '../interfaces/question-base-option';
import { QuestionBaseComponent } from './question-base';

export class QuestionDatePicker extends QuestionBaseComponent {
  constructor(options: QuestionBaseOption) {
    options.controlType = 'datePicker';
    super(options);
  }
}
