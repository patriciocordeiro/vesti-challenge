import { QuestionBaseOption } from '../interfaces/question-base-option';
import { QuestionBaseComponent } from './question-base';

export class  QuestionTextbox  extends QuestionBaseComponent {

  constructor(options: QuestionBaseOption) {
    options.controlType = 'textbox';
    super(options);
  }

}
