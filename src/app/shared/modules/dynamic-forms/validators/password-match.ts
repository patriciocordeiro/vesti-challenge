import { AbstractControl, ValidationErrors, ValidatorFn } from '@angular/forms';

export const checkPasswordMatch: ValidatorFn = (control: AbstractControl): ValidationErrors | null => {
  if (control) {
    const password = control.get('password');
    const retypePassword = control.get('retypePassword');
    if (password && retypePassword && ((password.touched || retypePassword.touched) && password.value !== retypePassword.value)) {
      retypePassword?.setErrors({ matchPassword: true });
      return { matchPassword: true };
    } else if ((password && retypePassword) && ((password.touched || retypePassword.touched) && password.value === retypePassword.value)) {
      retypePassword?.setErrors(null);
      return null;

    } else {
      return null;
    }
  } else {
    return null;
  }

};
