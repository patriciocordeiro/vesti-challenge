import { Validators } from '@angular/forms';

export class QuestionBaseOption {

  value: any;
  key: string;
  label: string;
  type?: 'text' | 'password' | 'email' | 'number' | 'search' | 'tel' | 'url';
  order: number;
  controlType?: string;
  disabled?: boolean;
  position?: string;
  placeholder?: string;
  mask?: string;
  validator?: Validators;
  format?: string;
  clearInput?: boolean;


}
