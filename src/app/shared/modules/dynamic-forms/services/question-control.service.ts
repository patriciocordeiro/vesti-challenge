import { Injectable } from '@angular/core';
import { FormControl, FormGroup, Validators } from '@angular/forms';

import { checkPasswordMatch } from '../validators/password-match';


@Injectable({
  providedIn: 'root'
})
export class QuestionControlService {
  constructor() { }

  toFormGroup(questions: any[], usePasswordMatchValidator = false) {
    const group: any = {};
    questions.forEach(question => {
      group[question.key] = new FormControl({ value: question.value, disabled: question.disabled } || '', question.validator);
    });
    const composeValidator = [];

    if (usePasswordMatchValidator) {
      composeValidator.push(checkPasswordMatch);
    }

    return new FormGroup(group, Validators.compose(composeValidator));

  }

  addQuestions(currentQuestions: any, newQuestions: any, formGroup: FormGroup): {
    formGroup: FormGroup;
    questions: any[];
  } {
    let questions = [...currentQuestions];
    const formKeys = Object.keys(formGroup.value);
    newQuestions.forEach((question: any) => {
      if (!formKeys.includes(question.key)) {
        questions = [...questions, question];
        formGroup.addControl(question.key, new FormControl(question.value || '', question.validator));
        formGroup.updateValueAndValidity();
      }
    });
    return { formGroup, questions };
  }
  removeQuestions(currentQuestions: any, newQuestionsToRemove: any, formGroup: FormGroup): {
    formGroup: FormGroup;
    questions: any[];
  } {
    let questions = [...currentQuestions];
    const formKeys = Object.keys(formGroup.value);
    newQuestionsToRemove.forEach((question: any) => {
      if (formKeys.includes(question.key)) {
        questions = questions.filter(e => (e.key !== question.key));
        formGroup.removeControl(question.key);
        formGroup.updateValueAndValidity();
      }
    });
    return { formGroup, questions };
  }
}
