import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { IonicModule } from '@ionic/angular';
import { SimpleMaskModule } from 'ngx-ion-simple-mask';

import { QuestionControlService } from './services/question-control.service';
import { DynamicFormQuestionComponent } from './templates/dynamic-form-question/dynamic-form-question.component';
import { DynamicFormComponent } from './templates/dynamic-form/dynamic-form.component';


@NgModule({
  imports: [
    CommonModule,
    IonicModule,
    FormsModule,
    ReactiveFormsModule,
    SimpleMaskModule
  ],
  declarations: [DynamicFormComponent, DynamicFormQuestionComponent],
  providers: [QuestionControlService],
  exports: [DynamicFormComponent, DynamicFormQuestionComponent]
})
export class DynamicFormModule { }
