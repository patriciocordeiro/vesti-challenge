import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { IonicModule } from '@ionic/angular';

import { SimpleListSkeletonComponent } from './simple-list-skeleton.component';

@NgModule({
  imports: [
    CommonModule,
    IonicModule
  ],
  declarations: [SimpleListSkeletonComponent],
  exports: [SimpleListSkeletonComponent]
})
export class SimpleListSkeletonModule { }
