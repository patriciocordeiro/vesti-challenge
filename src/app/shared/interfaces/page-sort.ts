export interface PageSort {
  active: string;
  direction: 'asc' | 'desc';
}
