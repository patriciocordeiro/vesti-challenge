export interface GeneralError {
  code: string;
  message: string;
}
