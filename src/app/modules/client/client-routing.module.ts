import { ClientListComponent } from './client-list/client-list.component';
import { Routes, RouterModule } from '@angular/router';
import { ClientDetailComponent } from './client-detail/client-detail.component';
import { NgModule } from '@angular/core';

const routes: Routes = [
  {
    path: '',
    children: [
      { path: '', component: ClientListComponent },

      {
        path: ':id',
        component: ClientDetailComponent
      }
    ]
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class ClientRoutingModule {}
