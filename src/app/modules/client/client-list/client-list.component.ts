import { Component, OnDestroy, OnInit } from '@angular/core';
import { LoadingController, ModalController, ToastController } from '@ionic/angular';
import { Store } from '@ngrx/store';
import { Observable, Subscription } from 'rxjs';
import { filter } from 'rxjs/operators';

import { Client } from '../shared/interfaces/client';
import { ClientService } from '../shared/services/client.service';
import { clearClientSaveOk, loadClients } from '../shared/store/client.actions';
import { selectAllClient, selectClientLoading, selectClientSavedOk } from '../shared/store/client.selectors';
import { ClientAddModalComponent } from './../client-add-modal/client-add-modal.component';



@Component({
  selector: 'app-client-list',
  templateUrl: './client-list.component.html',
  styleUrls: ['./client-list.component.scss']
})
export class ClientListComponent implements OnInit, OnDestroy {
  savedOk: boolean;



  slidingOptions: any[];
  clients$: Observable<Client[]>;
  loading$: Observable<boolean>;
  selectedItems: string[];
  skeletonRepeatItems = Array(10).fill(0);
  enableItemsEdit: boolean;

  private subscription = new Subscription();
  private loadingOverlay: HTMLIonAlertElement = null;

  constructor(
    public modalController: ModalController,
    private store: Store,
    private clientService: ClientService,
    public loadingController: LoadingController,
    private toastController: ToastController

  ) { }

  ngOnInit() {
    this.initStore();
    this.loadClients();
    this.listenForSaveOk();

  }

  ngOnDestroy(): void {
    this.subscription.unsubscribe();
  }


  async presentAddClientModal() {
    const modal = await this.modalController.create({
      component: ClientAddModalComponent,
    });
    return await modal.present();
  }




  deleteClient(clientId: string) {
    this.clientService.deleteClientHandler(clientId).then(overlay => this.loadingOverlay = overlay);
  }

  trackByFn(index: number, el: Client) {
    return el.id;
  }

  private initStore() {

    this.clients$ = this.store.select(selectAllClient);

    this.loading$ = this.store.select(selectClientLoading);
  }


  private loadClients() {
    this.store.dispatch(loadClients());
  }

  private async presentDeleteConfirmToast() {
    const toast = await this.toastController.create({
      message: 'Cliente excluido com sucesso',
      duration: 2000,
    });
    toast.present();
  }

  private listenForSaveOk() {
    this.subscription.add(this.store.select(selectClientSavedOk)
      .pipe(
        filter(el => el)).subscribe(() => {
          this.store.dispatch(clearClientSaveOk());
          if (this.loadingOverlay) {
            this.loadingController.dismiss();
          }
          this.presentDeleteConfirmToast();
        })
    );
  }
}
