import { Component, Input, OnDestroy, OnInit } from '@angular/core';
import { FormGroup } from '@angular/forms';
import { ModalController, ToastController } from '@ionic/angular';
import { Store } from '@ngrx/store';
import { from, Observable, Subscription } from 'rxjs';
import { filter, mergeMap, tap } from 'rxjs/operators';
import { QuestionControlService } from 'src/app/shared/modules/dynamic-forms/services/question-control.service';

import { addClient } from '../shared/store/client.actions';
import { ClientFormService } from './../shared/services/client-form.service';
import { clearClientSaveOk, updateClient } from './../shared/store/client.actions';
import { selectClientById, selectClientSavedOk, selectClientSaveLoading } from './../shared/store/client.selectors';



@Component({
  selector: 'app-client-add-modal',
  templateUrl: './client-add-modal.component.html',
  styleUrls: ['./client-add-modal.component.scss']
})
export class ClientAddModalComponent implements OnInit, OnDestroy {

  @Input() id: string;

  formGroup: FormGroup;
  questions!: any[];
  saveLoading$: Observable<boolean>;

  private subscription = new Subscription();

  constructor(
    private store: Store,
    private modalController: ModalController,
    private qcs: QuestionControlService,
    private clientFormService: ClientFormService,
    private toastController: ToastController,
  ) {
  }


  ngOnInit() {
    this.initStore();
    this.buildQuestionsFormGroup();
    this.listenForSaveOk();
    this.setForminitialValues();
  }

  ngOnDestroy(): void {
    this.subscription.unsubscribe();

  }



  saveHandler() {
    if (this.formGroup.invalid) {
      this.formGroup.markAllAsTouched();
    } else {
      if (this.id) {
        this.store.dispatch(updateClient({ id: this.id, changes: { ...this.formGroup.value } }));
      } else {
        this.store.dispatch(addClient({ data: { ...this.formGroup.value } }));
      }
    }
  }


  dismissModal() {
    this.modalController.dismiss();
  }

  private initStore() {
    this.saveLoading$ = this.store.select(selectClientSaveLoading);


  }


  private setForminitialValues() {

    if (this.id) {
      const questionsKeys = Object.keys(this.formGroup.controls);
      this.store.select(selectClientById(this.id)).pipe(
        filter(client => !!client),
        mergeMap((client => from(questionsKeys).pipe(
          tap(key => {
            this.formGroup.controls[key].setValue(client[key]);
          })
        )
        ))
      ).subscribe();
    }
  }



  private buildQuestionsFormGroup() {
    this.questions = this.clientFormService.newClientQuestions;
    this.formGroup = this.qcs.toFormGroup(this.questions);
  }


  private listenForSaveOk() {
    this.subscription.add(this.store.select(selectClientSavedOk)
      .pipe(
        filter(el => el)).subscribe(res => {
          this.store.dispatch(clearClientSaveOk());
          this.presentToast();
          this.dismissModal();
        })
    );
  }


  private async presentToast() {
    const toast = await this.toastController.create({
      message: this.id ? 'Cliente atualizado com sucesso' : 'Cliente adicionado com sucesso',
      duration: 2000,
    });
    toast.present();
  }


}
