import { Component, OnDestroy, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { LoadingController, ModalController, ToastController } from '@ionic/angular';
import { Store } from '@ngrx/store';
import { Observable, Subscription } from 'rxjs';
import { filter } from 'rxjs/operators';

import { ClientAddModalComponent } from '../client-add-modal/client-add-modal.component';
import { Client } from '../shared/interfaces/client';
import { clearClientSaveOk, loadOneClient } from '../shared/store/client.actions';
import { selectClientById, selectClientLoading, selectClientSavedOk } from '../shared/store/client.selectors';
import { ClientService } from './../shared/services/client.service';


@Component({
  selector: 'app-client-detail',
  templateUrl: './client-detail.component.html',
  styleUrls: ['./client-detail.component.scss']
})
export class ClientDetailComponent implements OnInit, OnDestroy {

  loading$: any;
  skeletonRepeatItems = Array(5).fill(0);
  selectedClient$: Observable<Client>;

  private subscription = new Subscription();
  private loadingOverlay: HTMLIonAlertElement = null;

  constructor(
    private store: Store,
    private modalController: ModalController,
    private toastController: ToastController,
    private router: Router,
    private activatedRoute: ActivatedRoute,
    private clientService: ClientService,
    private loadingController: LoadingController
  ) { }

  ngOnInit(): void {
    this.initStore();
    this.listenForSaveOk();
  }

  ngOnDestroy(): void {
    this.subscription.unsubscribe();
  }


  async presentAddClientModal(clientId: string) {
    const modal = await this.modalController.create({
      component: ClientAddModalComponent,
      componentProps: {
        id: clientId
      }
    });
    return await modal.present();
  }

  deleteClient(clientId: string) {
    this.clientService.deleteClientHandler(clientId).then(overlay => this.loadingOverlay = overlay);
  }


  private initStore() {
    this.loading$ = this.store.select(selectClientLoading);

    const id = this.activatedRoute.snapshot.params.id;
    this.store.dispatch(loadOneClient({ id }));
    this.selectedClient$ = this.store.select(selectClientById(id));
  }


  private async presentDeleteConfirmToast() {
    const toast = await this.toastController.create({
      message: 'Cliente excluido com sucesso',
      duration: 2000,
    });
    toast.present();
  }

  private listenForSaveOk() {
    this.subscription.add(this.store.select(selectClientSavedOk)
      .pipe(
        filter(el => el)).subscribe(res => {
          this.store.dispatch(clearClientSaveOk());
          if (this.loadingOverlay) {
            this.loadingController.dismiss();
          }
          this.router.navigate(['../'], { relativeTo: this.activatedRoute });
          this.presentDeleteConfirmToast();
        })
    );
  }

}
