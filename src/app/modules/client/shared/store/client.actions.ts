import { createAction, props } from '@ngrx/store';
import { GeneralError } from 'src/app/shared/interfaces/general-error';

import { Client } from '../interfaces/client';


export const loadClients = createAction('[Client] Load Clients',
);

export const loadClientsSuccess = createAction(
  '[Client] Load Clients Success',
  props<{ data: Client[]}>()
);

export const loadClientsFailure = createAction(
  '[Client] Load Clients Failure',
  props<{ error: GeneralError }>()
);

export const loadOneClient = createAction('[Client] Load One Client', props<{ id: string }>());
export const loadOneClientSuccess = createAction(
  '[Client] Load One Client Success',
  props<{ data: Client[] }>()
);
export const loadOneClientFailure = createAction(
  '[Client] Load One Client Failure',
  props<{ error: GeneralError }>()
);

export const loadClientFailure = createAction(
  '[Client] Load One Client Failure',
  props<{ error: GeneralError }>()
);

export const addClient = createAction('[Client] Add Client', props<{ data: Client}>());
export const addClientSuccess = createAction(
  '[Client] Add Client Success',
  props<{ data: Client }>()
);
export const addClientFailure = createAction(
  '[Client] Add Client Failure',
  props<{ error: GeneralError }>()
);

export const updateClient = createAction(
  '[Client] Update One Client',
  props<{ id: string; changes: Partial<Client>}>()
);
export const updateClientSuccess = createAction(
  '[Client] Update Client Success',
  props<{ data: Client }>()
);
export const updateClientFailure = createAction(
  '[Client] Update Client Failure',
  props<{ error: GeneralError }>()
);

export const deleteClient = createAction('[Client] Delete Client', props<{ id: string }>());
export const deleteClientSuccess = createAction(
  '[Client] Delete Client Success',
);
export const deleteClientFailure = createAction(
  '[Client] Delete Client Failure',
  props<{ error: GeneralError }>()
);


export const clearClientSaveOk = createAction(
  '[User] Clear Client Saved OK',
);

