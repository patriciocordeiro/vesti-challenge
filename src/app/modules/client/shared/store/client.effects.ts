import { Injectable } from '@angular/core';
import { Actions, createEffect, ofType } from '@ngrx/effects';
import { of } from 'rxjs';
import { catchError, map, switchMap } from 'rxjs/operators';

import { Client } from '../interfaces/client';
import { ClientApiService } from '../services/client-api.service';
import * as ClientActions from './client.actions';

/* eslint-disable arrow-body-style */
@Injectable()
export class ClientEffects {
  loadClients$ = createEffect(() => {
    return this.actions$.pipe(
      ofType(ClientActions.loadClients),
      switchMap(() => this.clientApiService.watchAll<Client>().pipe(
        map(data => ClientActions.loadClientsSuccess({ data })),
        catchError(error => of(ClientActions.loadClientsFailure({ error })))
      )));
  });

  loadOneClient = createEffect(() => {
    return this.actions$.pipe(
      ofType(ClientActions.loadOneClient),
      switchMap(payload => this.clientApiService.getOne<Client>(payload.id).pipe(
        map(data => ClientActions.loadOneClientSuccess({ data })),
        catchError(error => of(ClientActions.loadOneClientFailure({ error })))
      )));
  });


  addClient$ = createEffect(() => {
    return this.actions$.pipe(
      ofType(ClientActions.addClient),
      switchMap(payload => this.clientApiService.create<Client>(payload.data).pipe(
        map(data => ClientActions.addClientSuccess({ data })),
        catchError(error => of(ClientActions.addClientFailure({ error })))
      )));
  });

  updateClient$ = createEffect(() => {
    return this.actions$.pipe(
      ofType(ClientActions.updateClient),
      switchMap(payload => this.clientApiService.update(payload.id, payload.changes).pipe(
        map(data => ClientActions.updateClientSuccess({ data })),
        catchError(error => of(ClientActions.updateClientFailure({ error })))
      )));
  });

  deleteClient$ = createEffect(() => {
    let id: string;
    return this.actions$.pipe(
      ofType(ClientActions.deleteClient),
      switchMap(payload => this.clientApiService.delete(payload.id).pipe(
        map(() => ClientActions.deleteClientSuccess()),
        catchError(error => of(ClientActions.deleteClientFailure({ error }))
        )
      )
      )
    );
  });


  constructor(
    private actions$: Actions,
    private clientApiService: ClientApiService,
  ) { }
}
