import { createEntityAdapter, EntityAdapter, EntityState } from '@ngrx/entity';
import { Action, createReducer, on } from '@ngrx/store';

import { Client } from '../interfaces/client';
import { GeneralError } from './../../../../shared/interfaces/general-error';
import * as ClientActions from './client.actions';

export const clientFeatureKey = 'client';

export interface State extends EntityState<Client> {
  loading: boolean;
  saveLoading: boolean;
  loaded: boolean;
  selected: Client;
  error: GeneralError;
  savedOk: boolean;

}

export const adapter: EntityAdapter<Client> = createEntityAdapter<Client>({});

export const initialState: State = adapter.getInitialState({
  loading: false,
  saveLoading: false,
  loaded: false,
  selected: null as Client,
  savedOk: false,
  error: null
});

export const { selectAll, selectEntities, selectIds, selectTotal } = adapter.getSelectors();

const clientReducer = createReducer(
  initialState,
  /*** Load all */
  on(ClientActions.loadClients, (state): State => ({ ...state, ...{ loading: true, loaded: false } })),
  on(ClientActions.loadClientsSuccess, (state, action) => adapter.setAll(action.data,
    { ...state, ...{ loading: false, loaded: true, error: null } })
  ),
  on(ClientActions.loadClientsFailure, (state, action): State =>
  ({
    ...state, ...{ loading: false, loaded: true, error: action.error }
  })),

  /*** Load one */
  on(ClientActions.loadOneClient, (state): State => ({ ...state, ...{ loading: true } })),
  on(ClientActions.loadOneClientSuccess, (state, action) =>
    adapter.addOne(action?.data[0], { ...state, ...{ loading: false, error: null } })),
  on(ClientActions.loadOneClientFailure, (state, action): State => ({
    ...state,
    ...{ loading: false, error: action.error }
  })),
  /*** Add one */
  on(ClientActions.addClient, (state): State => ({ ...state, ...{ saveLoading: true } })),
  on(ClientActions.addClientSuccess, (state, action): State => ({
    ...state,
    ...{ saveLoading: false, savedOk: true, error: null }
  })
  ),
  on(ClientActions.addClientFailure, (state, action): State => ({
    ...state,
    ...{ saveLoading: false, error: action.error }
  })),
  /*** Update */
  on(ClientActions.updateClient, (state): State => ({ ...state, ...{ saveLoading: true } })),
  on(ClientActions.updateClientSuccess, (state, action): State => ({
    ...state,
    ...{ saveLoading: false, savedOk: true, error: null }
  })
  ),
  on(ClientActions.updateClientFailure, (state, action): State => ({
    ...state,
    ...{ saveLoading: false, error: action.error }
  })),
  /*** Delete */
  on(ClientActions.deleteClient, (state): State => ({ ...state, ...{ saveLoading: true } })),
  on(ClientActions.deleteClientSuccess, (state, action): State => ({
    ...state,
    ...{ saveLoading: false, savedOk: true, error: null }
  })
  ),
  on(ClientActions.deleteClientFailure, (state, action): State => ({
    ...state,
    ...{ saveLoading: false, error: action.error }
  })),

  on(ClientActions.clearClientSaveOk, (state): State => ({
    ...state,
    ...{ savedOk: false }
  })),

);
export const reducer = (state: State | undefined, action: Action) => clientReducer(state, action);
