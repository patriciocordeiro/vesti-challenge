import { createFeatureSelector, createSelector } from '@ngrx/store';

import * as fromClient from './client.reducer';

export const selectClientState = createFeatureSelector<fromClient.State>(
  fromClient.clientFeatureKey
);

export const selectEntities = createSelector(
  selectClientState,
  fromClient.selectEntities
);
export const selectAllClient = createSelector(selectClientState, state => fromClient.selectAll(state)
);

export const selectClientById = (id: string) =>
  createSelector(selectClientState, state => state.entities[id]);

export const selectClientLoading = createSelector(selectClientState, state => state.loading);
export const selectClientSaveLoading = createSelector(selectClientState, state => state.saveLoading);

export const selectClientSavedOk = createSelector(selectClientState, state => state.savedOk);

export const selectClientLoaded = createSelector(selectClientState, state => state.loaded);
