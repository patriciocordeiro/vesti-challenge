import { Injectable } from '@angular/core';
import { AngularFirestore } from '@angular/fire/firestore';
import { FirebaseDatabaseService } from 'src/app/shared/services/firebase-database.service';

@Injectable({
  providedIn: 'root'
})
export class ClientApiService extends FirebaseDatabaseService {
  constructor(public angularFirestore: AngularFirestore) {
    super(angularFirestore, 'client', { active: 'title', direction: 'desc' },
    );
  }

}
