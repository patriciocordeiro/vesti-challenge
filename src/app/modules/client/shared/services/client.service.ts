import { Injectable } from '@angular/core';
import { AlertController, LoadingController } from '@ionic/angular';
import { Store } from '@ngrx/store';

import { deleteClient } from '../store/client.actions';


@Injectable({
  providedIn: 'root'
})
export class ClientService {
  constructor(
    public alertController: AlertController,
    public loadingController: LoadingController,
    private store: Store
  ) {
  }



  async deleteClientHandler(clientId: string) {
    const alert = await this.alertController.create({
      header: 'Excluir cliente',
      message: 'Confirma excluir este cliente?',
      buttons: [
        {
          text: 'Cancelar',
          role: 'cancel',
          handler: () => {
          }
        }, {
          text: 'Confirmar',
          handler: () => {
            this.presentSimpleLoading().then();
            this.store.dispatch(deleteClient({ id: clientId }));;
          }
        }
      ]
    });

    await alert.present();
    return alert;
  }


  async presentSimpleLoading() {
    const loading = await this.loadingController.create({
      translucent: true,
    });
    await loading.present();

  }
}
