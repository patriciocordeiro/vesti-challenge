import { Injectable } from '@angular/core';
import { Validators } from '@angular/forms';
import { QuestionDatePicker } from 'src/app/shared/modules/dynamic-forms/factory/question-datepicker';
import { QuestionTextbox } from 'src/app/shared/modules/dynamic-forms/factory/question-textbox';

@Injectable({
  providedIn: 'root'
})
export class ClientFormService {

  constructor() { }


  get newClientQuestions() {


    const qt = [
      new QuestionTextbox({
        key: 'name',
        label: 'Nome do usuário',
        placeholder: 'Nome do usuário',
        position: 'floating',
        order: 1,
        type: 'text',
        clearInput: true,
        value: null,
        validator: Validators.required
      }),

      new QuestionTextbox({
        key: 'email',
        label: 'Email do usuário',
        placeholder: 'Email do usuário',
        position: 'floating',
        order: 2,
        type: 'email',
        value: null,
        clearInput: true,
        validator:  Validators.compose([Validators.email, Validators.required])
      }),
      new QuestionTextbox({
        key: 'phone',
        label: 'Telefone',
        placeholder: '(11) 912345-6789',
        position: 'floating',
        mask: '(99) 99999-9999',
        order: 3,
        type: 'tel',
        value: null,
        clearInput: true,
        validator: Validators.compose([
          Validators.minLength(15),
          Validators.maxLength(15),
          Validators.required
        ])
      }),

      new QuestionDatePicker({
        key: 'birthDate',
        label: 'Data de nascimento',
        position: 'floating',
        order: 4,
        value: null,
        validator: Validators.required
      }),


    ];

    return qt.sort((a, b) => a.order - b.order);
  };
}
