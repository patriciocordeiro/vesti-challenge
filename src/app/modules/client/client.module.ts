import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { IonicModule } from '@ionic/angular';
import { EffectsModule } from '@ngrx/effects';
import { StoreModule } from '@ngrx/store';

import { DynamicFormModule } from './../../shared/modules/dynamic-forms/dynamic-form.module';
import { SimpleListSkeletonModule } from './../../shared/modules/simple-list-skeleton/simple-list-skeleton.module';
import { ClientAddModalComponent } from './client-add-modal/client-add-modal.component';
import { ClientDetailComponent } from './client-detail/client-detail.component';
import { ClientListComponent } from './client-list/client-list.component';
import { ClientRoutingModule } from './client-routing.module';
import { ClientEffects } from './shared/store/client.effects';
import * as fromClient from './shared/store/client.reducer';

@NgModule({
  imports: [
    CommonModule,
    ClientRoutingModule,
    ReactiveFormsModule,
    FormsModule,
    IonicModule,
    DynamicFormModule,
    SimpleListSkeletonModule,
    StoreModule.forFeature(fromClient.clientFeatureKey, fromClient.reducer),
    EffectsModule.forFeature([ClientEffects])
  ],
  declarations: [ClientListComponent, ClientDetailComponent, ClientAddModalComponent],
  entryComponents: [ClientAddModalComponent]


})
export class ClientModule { }
