# Vesti challenge: Aplicativo de Controle de clientes
Projeto de um aplicativo simples para controle de clientes. O aplicativo oferece ao usuário a listagem, cadastro, edição e exclusão de clientes.

Este projeto usa [Ionic 5](https://ionicframework.com/) com [ Angular 12](https://angular.io/), [Firebase](https://firebase.google.com/) Firestore como banco de dados em tempo real. O projeto usa também o [NGRX](https://ngrx.io/) como store.

## Instalação

Baixe o repositótio e execute:
`npm install`

## Execução

Abra o terminal na pasta de projeto e execute:
`npm run start`
